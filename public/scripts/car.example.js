class Car {
    static list = [];

    static init(cars) {
        this.list = cars.map((i) => new this(i));
    }

    constructor({ id, plate, manufacture, model, image, rentPerDay, capacity, description, transmission, available, type, year, options, specs, availableAt }) {
        this.id = id;
        this.plate = plate;
        this.manufacture = manufacture;
        this.model = model;
        this.image = image;
        this.rentPerDay = rentPerDay;
        this.capacity = capacity;
        this.description = description;
        this.transmission = transmission;
        this.available = available;
        this.type = type;
        this.year = year;
        this.options = options;
        this.specs = specs;
        this.availableAt = availableAt;
    }

    render() {
        return `
          <div class="card card-rent col-lg-4 col-md-6">
            <div class="card-body">
              <img class="img-thumbnail" src="${this.image}" alt="">
              <p class="card-text mt-4">${this.manufacture} ${this.model}</p>
              <p class="card-text fw-bold" style="font-size: 16px;">Rp. ${this.rentPerDay} / hari</p>
              <p class="card-text">${this.description}</p>
              <p class="card-text"><img class="me-2" src="images/fi_users.png" alt="">${this.capacity}</p>
              <p class="card-text"><img class="me-2" src="images/fi_settings.png" alt="">${this.transmission}</p>
              <p class="card-text"><img class="me-2" src="images/fi_calendar.png" alt="">${this.year}</p>
              <button type="submit" class="btn btn-success container-fluid">Pilih Mobil</button>
            </div>
          </div>
    `;
        /*  <p>id: <b>${this.id}</b></p>
    <p>plate: <b>${this.plate}</b></p>
    <p>manufacture: <b>${this.manufacture}</b></p>
    <p>model: <b>${this.model}</b></p>
    <p>capacity: <b>${this.capacity}</b></p>
    <p>available at: <b>${this.availableAt}</b></p>
    <img src="${this.image}" alt="${this.manufacture}" width="64px">
 */
    }
}
