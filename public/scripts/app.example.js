class App {
     constructor() {
        this.clearButton = document.getElementById("clear-btn");
        this.loadButton = document.getElementById("load-btn");
        // this.searchButton = document.getElementById("search-btn");
        this.carContainerElement = document.getElementById("cars-container");
        this.dateRent = document.getElementById("date-rent");
        this.timeRent = document.getElementById("time-rent");
        this.passengerRent = document.getElementById("passenger");
    }

    async init() {
        await this.load();

        // Register click listener
        this.clearButton.onclick = this.clear;
        this.loadButton.onclick = this.run;
        // this.searchButton.onclick = this.run;
    }

    run = async () => {
        await this.load(); // karena async jadi di tambahkan await
        this.clear()
        Car.list.forEach((car) => {
            const node = document.createElement("div");
            node.innerHTML = car.render();
            this.carContainerElement.appendChild(node);
        });
    };

    async load() {
        const tanggal = this.dateRent.value;
        const jam = this.timeRent.value;
        const penumpang = this.passengerRent.value;

        console.log("tanggal", tanggal);
        console.log("jam", jam);
        console.log("penumpang", penumpang);

        const dateTime = new Date(`${tanggal} ${jam}`);
        console.log("tanggalJam", dateTime);

        const epochTime = dateTime.getTime(); // methode getTime mengubah obj dateTime ke milisecond 
        console.log("epochTime", epochTime);

        const cars = await Binar.listCars((item) => {
            const filterByCapacity = item.capacity = penumpang; // filter kapasitas penumpang
            const filterByDateRent = item.availableAt.getTime() < epochTime; // filter tanggal
            return filterByCapacity && filterByDateRent;
        });
        Car.init(cars);
    }

    clear = () => {
        let child = this.carContainerElement.firstElementChild;

        while (child) {
            child.remove();
            child = this.carContainerElement.firstElementChild;
        }
    };
}
